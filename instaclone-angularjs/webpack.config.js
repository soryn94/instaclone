require('babel-polyfill');
const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");

let config = {
    entry: {
        app: ['babel-polyfill', path.join(__dirname, './app/index.js')],
        vendor: [
            'angular',
            '@uirouter/angularjs'
        ]
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].[chunkhash].js'
    },
    module: {
        loaders: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['es2015']
            }
        },
        { 
            test: /\.html$/, loader: "html-loader" 
        },
        {
            test: /\.(css|scss)$/,
            loader: 'style-loader!css-loader!sass-loader'
        },
        {   test: /\.(png|woff|woff2|eot|ttf|svg)$/,
            loader: 'url-loader?limit=100000'
        }
      ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./app/index.html",
            inject: "body",
            chunks: ['vendor', 'app'],
            chunksSortMode: 'manual'
        })
    ]
}

module.exports = config;