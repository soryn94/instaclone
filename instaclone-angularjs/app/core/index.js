import routing from './routing';

import { NAME as NAVIGATOR_NAME, component as navigatorComponent } from './navigator/navigator.component';
import { NAME as PROFILE_NAME, component as profileComponent } from './profile/profile.component';

import { RequestService } from './request.service';
import { ProfileService } from './profile.service';

export default angular.module('instaclone.core', [])
    .config(routing)
    .service('requestService', RequestService)
    .service('profileService', ProfileService)
    .component(NAVIGATOR_NAME, navigatorComponent)
    .component(PROFILE_NAME, profileComponent)
    .name