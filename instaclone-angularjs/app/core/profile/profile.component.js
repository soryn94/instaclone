import template from './profile.component.html';

class Profile {
    constructor(profileService) {
        this.profileService = profileService;
        this.ec = 'asdasdad'
        this.profile = {
            image: "https://scontent-otp1-1.cdninstagram.com/vp/d47bb8fb879f88e5ddd361633c524aa7/5BA488D7/t51.2885-19/s150x150/22638771_138322276895570_2661938283868585984_n.jpg",
            name: "ebsintegrator",
            info: "EBS Integrator ▫️iOS App Development ▫️Android App Development ▫️Web Development",
            link: "www.ebs-integrator.com",
            stats: {
                posts: 36,
                followers: 102,
                following: 118
            }
        }
    }

    $onInit(){
        // getPhotos()
    }

    getPhotos() {
        this.profileService.getPhotos(0, 12, "latest")
            .then((response) => console.log(response))
    }
}

Profile.$inject = ['profileService'];

export const NAME = 'profile';

export const component = {
    controller: Profile,
    template: template,
};