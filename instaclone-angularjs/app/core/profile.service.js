import Unsplash, { toJson } from 'unsplash-js';

export class ProfileService {
    constructor(requestService, config) {
        this.request = requestService;
        this.unsplash = new Unsplash(config)
    }

    getPhotos(page, perPage, orderBy) {
        console.log(page, perPage, orderBy)
        return this.unsplash.photos.listPhotos(page, perPage, orderBy).then(toJson)
    }
}

ProfileService.$inject = ['requestService', 'config'];
