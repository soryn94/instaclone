routing.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

export default function routing($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/profile');
    $urlRouterProvider.when('', '/profile');
    
    $stateProvider
        .state('nav', {
            template: '<navigator></navigator>',
            url: '',
        })
        .state('profile', {
            parent: 'nav',
            template: '<profile></profile>',
            url: '/profile'
        })
}