export class RequestService {
    constructor($http, config) {
        this.http = $http;
        this.config = config;

    }

    get(endpoint, data) {
        return this._request({
            url: this._endpoint(endpoint),
            method: 'GET',
            params: data,
            headers: {
                "Cache-Control": 'no-cache',
                "Pragma": 'no-cache',
                "Expires": 'Sat, 01 Jan 2000 00:00:00 GMT'
            }
        });
    }

    post(endpoint, data) {
        return this._request({
            url: this._endpoint(endpoint),
            method: 'POST',
            data: data
        });
    }

    put(endpoint, data) {
        return this._request({
            url: this._endpoint(endpoint),
            method: 'PUT',
            data: data
        });
    }

    delete(endpoint, data) {
        return this._request({
            url: this._endpoint(endpoint),
            method: 'DELETE',
            data: data,
            headers : {
                "Content-Type": "application/json;charset=utf-8"
            }
        });
    }

    _request(config) {

        return this.http(config).then((response) => {
            if (response.data.error) {
                return Promise.reject(response.data.error);
            }

            return Promise.resolve(response.data);
        }, (response) => {
            return Promise.reject(response.data.error);
        });
    }

    _endpoint(path) {
        return this.config.endpoint + path;
    }
}

RequestService.$inject = ['$http', 'config'];
