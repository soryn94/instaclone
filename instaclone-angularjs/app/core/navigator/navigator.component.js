import template from './navigator.component.html';

class Navigator {
    constructor() {}

}

Navigator.$inject = [];

export const NAME = 'navigator';

export const component = {
    controller: Navigator,
    template: template,
};