import coreName from './core';

import dependencies from './dependencies.js';

import config from './config.json';
import './index.scss';

let applicationModules = dependencies.concat([coreName])
let applicationName = angular.module('instaclone', applicationModules)
    .value('config', config).name;

angular.bootstrap(document.body, [applicationName]);